package maze

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.system.measureTimeMillis

fun main() {
    val resourceFolder = Paths.get("src/main/resources/").toAbsolutePath().toString()
    val instancesFolder = Paths.get(resourceFolder, "unsolved")

    Files.walk(instancesFolder)
        .filter { Files.isRegularFile(it) }
        .filter { it.toString().endsWith(".png") }
        .filter{ !it.toString().contains("6") && !it.toString().contains("5")}
        .forEach { benchmark(it) }
}

fun benchmark(filePath: Path?) {
    println("Instance ${filePath.toString().replaceBeforeLast(File.separator, "")}")

    var m: Maze
    val buildTime = measureTimeMillis { m = Maze(filePath.toString()) }

    val dijkstraTime = measureTimeMillis { m.solve(Maze.Strategies.DIJKSTRA, true) }
    val aStarTime = measureTimeMillis { m.solve(Maze.Strategies.ASTAR, true) }
    val dfsTime = measureTimeMillis { m.solve(Maze.Strategies.DFS, true) }
    val bfsTime = measureTimeMillis { m.solve(Maze.Strategies.BFS, true) }

    println("\t%.2f sec\tGraph build time".format(buildTime / 1000f))
    println("\t%.2f sec\tDijkstra".format(dijkstraTime / 1000f))
    println("\t%.2f sec\tA*".format(aStarTime / 1000f))
    println("\t%.2f sec\tDFS".format(dfsTime / 1000f))
    println("\t%.2f sec\tBFS".format(bfsTime / 1000f))
}
