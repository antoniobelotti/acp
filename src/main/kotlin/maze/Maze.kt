package maze

import base.ShortestPathAlgorithms
import base.UndirectedGraph
import kotlinx.coroutines.*
import java.awt.Color
import java.awt.color.ColorSpace
import java.awt.image.BufferedImage
import java.awt.image.ColorConvertOp
import java.io.File
import java.lang.Integer.min
import java.lang.Integer.max
import java.nio.file.Paths
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.imageio.ImageIO
import kotlin.math.abs


class Maze(
    private val imgAbsPath: String,
    private val saveDetectedVertices: Boolean = false,
    private val useThreads: Boolean = true
) {
    enum class Strategies { ASTAR, DIJKSTRA, DFS, BFS }

    private var basePath = imgAbsPath.replaceAfterLast("${File.separator}resources", "")
    private var instanceName = imgAbsPath.split(File.separator).last().replace(".png", "")

    /* BufferedImage representation */
    private lateinit var originalBufferedImage: BufferedImage

    /* Pixel matrix representation */
    private lateinit var imagePixels: Array<IntArray>

    /* Graph representation */
    private lateinit var graph: UndirectedGraph<Pair<Int, Int>>

    init {
        loadBufferedImage()
        loadImageMatrix()
        //buildTrivialGraph()
        //reduceTrivialGraph()
        buildGraph()
        if (!instanceName.contains("braid"))
            reduceGraph()
    }

    private fun reduceGraph() {
        fun recRemove(n: Pair<Int, Int>) {
            // get current node neighbour
            val neighbour = graph.edges[n]?.get(0)?.first
            // remove arc neighbour ---> currentNode
            graph.edges[neighbour]!!.removeIf { (node, _) -> node == n }
            // remove current node edges and vertex
            graph.edges.remove(n)
            graph.vertices.remove(n)

            if (graph.edges[neighbour]!!.size == 1)
                recRemove(neighbour!!)
        }

        val prevNodeCount = graph.vertices.size
        val targets = mutableListOf<Pair<Int, Int>>()
        val s = getStartNode()
        val e = getExitNode()
        graph.edges.forEach { (n, listOfNodes) ->
            if (listOfNodes.size == 1 && n != s && n != e) {
                targets.add(n)
            }
        }

        targets.forEach { recRemove(it) }
        val percDecrease = (graph.vertices.size - prevNodeCount).toFloat() / prevNodeCount.toFloat() * 100
        println("Reduced from ${prevNodeCount} to ${graph.vertices.size} vertices. ${percDecrease}% reduction")
    }

    private fun loadBufferedImage() {
        val f = File(imgAbsPath)
        originalBufferedImage = ImageIO.read(f)
    }

    private fun loadImageMatrix() {
        imagePixels = Array(originalBufferedImage.height) { IntArray(originalBufferedImage.width) { 0 } }

        (0 until originalBufferedImage.height).forEach { rowIdx ->
            (0 until originalBufferedImage.width).forEach { colIdx ->
                imagePixels[rowIdx][colIdx] = originalBufferedImage.getRGB(colIdx, rowIdx)
            }
        }
    }

    private fun buildGraph() {
        /* add entry and exit edges otherwise the won't be detected */
        val s = getStartNode()
        val e = getExitNode()
        graph = UndirectedGraph(mutableListOf(s, e), listOf())

        fun scanRow(rowIdx: Int) {
            /* insert row nodes and edges */
            var prevNode: Pair<Int, Int>? = null

            val vertices = mutableListOf<Pair<Int, Int>>()
            val edges = mutableListOf<Triple<Pair<Int, Int>, Pair<Int, Int>, Float>>()

            (1 until imagePixels[0].size - 1).forEach { colIdx ->
                if (imagePixels[rowIdx][colIdx] == Color.BLACK.rgb) {
                    prevNode = null
                } else if (formsLShape(rowIdx, colIdx)) {
                    val currentNode = Pair(rowIdx, colIdx)
                    //graph.addVertices(currentNode)
                    vertices.add(currentNode)
                    if (prevNode != null) {
                        edges.add(Triple(prevNode!!, currentNode, (colIdx - prevNode!!.second).toFloat()))
                    }
                    prevNode = currentNode
                }
            }
            graph.addVertices(*vertices.toTypedArray())
            graph.addEdges(*edges.toTypedArray())
        }

        fun scanCols(colIdx:Int){
            /* insert col edges */
            var prevNode: Pair<Int, Int>? = null
            val edges = mutableListOf<Triple<Pair<Int, Int>, Pair<Int, Int>, Float>>()
            imagePixels.indices.forEach { rowIdx ->
                if (imagePixels[rowIdx][colIdx] == Color.BLACK.rgb) {
                    prevNode = null
                } else if (graph.vertices.contains(Pair(rowIdx, colIdx))) {
                    val currentNode = Pair(rowIdx, colIdx)
                    if (prevNode != null) {
                        edges.add(Triple(prevNode!!, currentNode, (rowIdx - prevNode!!.first).toFloat()))
                    }
                    prevNode = currentNode
                }
            }
            graph.addEdges(*edges.toTypedArray())
        }

        if (useThreads) {
            val rowExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())
            (1 until imagePixels.size - 1).forEach { rowIdx ->
                val worker = Runnable { scanRow(rowIdx) }
                rowExecutor.execute(worker)
            }
            rowExecutor.shutdown()
            rowExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES)

            val colExecutor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())
            /* add vertical edges */
            (1 until imagePixels[0].size - 1).forEach { colIdx ->
                val worker = Runnable { scanCols(colIdx) }
                colExecutor.execute(worker)
            }
            colExecutor.shutdown()
            colExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES)
        } else {
            (1 until imagePixels.size - 1).forEach { rowIdx ->
                scanRow(rowIdx)
            }
            (1 until imagePixels[0].size - 1).forEach { colIdx ->
                scanCols(colIdx)
            }
        }

        if (saveDetectedVertices)
            saveDetectedVerticesImage()
    }

    private fun formsLShape(r: Int, c: Int): Boolean {
        return ((imagePixels[r][c - 1] == Color.WHITE.rgb && imagePixels[r - 1][c] == Color.WHITE.rgb)
                || (imagePixels[r - 1][c] == Color.WHITE.rgb && imagePixels[r][c + 1] == Color.WHITE.rgb)
                || (imagePixels[r][c + 1] == Color.WHITE.rgb && imagePixels[r + 1][c] == Color.WHITE.rgb)
                || (imagePixels[r + 1][c] == Color.WHITE.rgb && imagePixels[r][c - 1] == Color.WHITE.rgb))
    }

    private fun buildTrivialGraph() {
        println("Building trivial graph")
        /* A node is represented by Pair<x,y> where x,y are the coordinates of the pixel */
        val vertices = hashSetOf<Pair<Int, Int>>()
        /* An edge is in the form <Node,Node,Weight> which translate to Triple<Pair<Int,Int>, Pair<Int,Int>, Float> */
        val edges = mutableListOf<Triple<Pair<Int, Int>, Pair<Int, Int>, Float>>()

        (0 until imagePixels.size - 1).forEach { rowIdx ->
            (0 until imagePixels[0].size - 1).forEach { colIdx ->
                if (imagePixels[rowIdx][colIdx] == Color.WHITE.rgb) {
                    vertices.add(Pair(rowIdx, colIdx))
                    // check next in-line pixel
                    if (imagePixels[rowIdx][colIdx + 1] == Color.WHITE.rgb) {
                        edges.add(Triple(Pair(rowIdx, colIdx), Pair(rowIdx, colIdx + 1), 1f))
                        vertices.add(Pair(rowIdx, colIdx + 1))
                    }

                    // check pixel under current pixel
                    if (imagePixels[rowIdx + 1][colIdx] == Color.WHITE.rgb) {
                        edges.add(Triple(Pair(rowIdx, colIdx), Pair(rowIdx + 1, colIdx), 1f))
                        vertices.add(Pair(rowIdx + 1, colIdx))
                    }
                }
            }
        }
        this.graph = UndirectedGraph(vertices.toMutableList(), edges)
        println("trivial graph done")
    }

    private fun reduceTrivialGraph() {
        println("Reducing graph")
        println("\tbefore: ${graph.vertices.size} nodes")
        graph.edges.forEach { (srcNode, outEdges) ->
            if (outEdges.size == 2) {
                val (e1, e2) = outEdges
                if (nodesAreOnTheSameAxis(e1.first, e2.first)) {
                    // src node can be removed:  e1 --1-- srcNode --1-- e2  becomes  e1 ---2--- e2
                    graph.edges[e1.first]!!.removeIf { it.first == srcNode }
                    graph.edges[e2.first]!!.removeIf { it.first == srcNode }

                    graph.addEdges(Triple(e1.first, e2.first, e1.second + e2.second))
                    graph.vertices.remove(srcNode)
                }
            }
        }
        println("\tafter: ${graph.vertices.size} nodes")
    }

    private fun nodesAreOnTheSameAxis(n: Pair<Int, Int>, p: Pair<Int, Int>): Boolean =
        n.first == p.first || n.second == p.second

    private fun getStartNode(): Pair<Int, Int> {
        val colIdx = imagePixels[0].indexOfFirst { it == Color.WHITE.rgb }
        return Pair(0, colIdx)
    }

    private fun getExitNode(): Pair<Int, Int> {
        val colIdx = imagePixels.last().indexOfFirst { it == Color.WHITE.rgb }
        return Pair(imagePixels.size - 1, colIdx)
    }

    fun solve(algorithm: Strategies, save: Boolean = false) {
        fun manhattanDistance(e1: Pair<Int, Int>, e2: Pair<Int, Int>) =
            (abs(e1.first - e2.first) + abs(e1.second - e2.second)).toFloat()

        val path = when (algorithm) {
            Strategies.DFS -> graph.depthFirstReturnPath(getStartNode(), getExitNode())!!
            Strategies.BFS -> graph.breadthFirstReturnPath(getStartNode(), getExitNode())
            Strategies.ASTAR -> graph.shortestPath(
                getStartNode(),
                getExitNode(),
                ShortestPathAlgorithms.ASTAR,
                ::manhattanDistance
            ).first
            Strategies.DIJKSTRA -> graph.shortestPath(
                getStartNode(),
                getExitNode(),
                ShortestPathAlgorithms.DIJKSTRA
            ).first
        }

        if (save)
            saveSolution(path, algorithm.toString())
    }

    private fun saveDetectedVerticesImage() {
        val op = ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_sRGB), null)
        val image = ImageIO.read(File(imgAbsPath))
        val correctImage = op.filter(image, null)

        graph.vertices.forEach { coords ->
            correctImage.setRGB(coords.second, coords.first, Color.ORANGE.rgb)
        }

        val outFilePath = Paths.get(basePath, "detected_vertices/${instanceName}.png").toString()
        ImageIO.write(correctImage, "png", File(outFilePath))
    }

    private fun saveSolution(path: List<Pair<Int, Int>>, solverName: String) {
        val op = ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_sRGB), null)
        val image = ImageIO.read(File(imgAbsPath))
        val correctImage = op.filter(image, null)

        (0 until path.size - 1).forEach { idx ->
            val (x1, y1) = path[idx]
            val (x2, y2) = path[idx + 1]

            /* nodes are on the same row */
            if (x1 == x2) {
                (min(y1, y2) until max(y1, y2) + 1).forEach { correctImage.setRGB(it, x1, Color.GREEN.rgb) }
            }

            /* nodes are on the same column */
            if (y1 == y2) {
                (min(x1, x2) until max(x1, x2) + 1).forEach { correctImage.setRGB(y1, it, Color.GREEN.rgb) }
            }
        }

        val outPath = Paths.get(basePath, "solved/${instanceName}_${solverName}.png").toString()
        ImageIO.write(correctImage, "png", File(outPath))
    }

}