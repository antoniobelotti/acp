package base

import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.HashSet

enum class ShortestPathAlgorithms { ASTAR, DIJKSTRA }

class UndirectedGraph<T>(
    elements: MutableList<T>,
    edges: List<Triple<T, T, Float>>
) : Graph<T>(elements, edges, true) {

    fun shortestPath(source: T, destination: T, strategy: ShortestPathAlgorithms, heuristic: ((T, T) -> Float)? = null)
            : Pair<List<T>, Float> {
        val (costs, predecessor) = when (strategy) {
            ShortestPathAlgorithms.DIJKSTRA -> dijkstraOrAStar(source)
            ShortestPathAlgorithms.ASTAR -> dijkstraOrAStar(source, destination, heuristic!!)
        }

        var cur = destination
        var result = mutableListOf(destination)
        while (predecessor[cur] != source) {
            // path does not exist
            if (!predecessor.containsKey(cur))
                return Pair(listOf(), Float.POSITIVE_INFINITY)

            result = (listOf(predecessor[cur]!!) + result).toMutableList()
            cur = predecessor[cur]!!
        }
        result = (listOf(predecessor[cur]!!) + result).toMutableList()
        return Pair(result, costs[destination]!!)
    }

    private fun dijkstraOrAStar(source: T, dest: T? = null, heuristic: ((T, T) -> Float)? = null)
            : Pair<HashMap<T, Float>, HashMap<T, T>> {

        val algo = if (dest != null && heuristic != null)
            ShortestPathAlgorithms.ASTAR
        else
            ShortestPathAlgorithms.DIJKSTRA

        val visited = HashSet<T>()
        visited.add(source)

        val pq = PriorityQueue(vertices.size, this::edgeComparator)

        val prev = HashMap<T, T>()
        val dist = HashMap<T, Float>()

        dist[source] = 0f
        when (algo) {
            ShortestPathAlgorithms.DIJKSTRA -> pq.add(Pair(source, 0f))
            ShortestPathAlgorithms.ASTAR -> pq.add(Pair(source, heuristic!!(source, dest!!)))
        }

        vertices
            .filter { it != source }
            .forEach {
                dist[it] = Float.POSITIVE_INFINITY
                pq.add(Pair(it, Float.POSITIVE_INFINITY))
            }

        while (pq.size > 0) {
            val node = pq.poll().first

            // disconnected component case
            if (edges[node] == null)
                continue
            edges[node]!!
                .filter { !visited.contains(it.first) }
                .forEach { (neighbourNode, weight) ->
                    visited.add(neighbourNode)

                    val alt = dist[node]?.plus(weight)
                    val tmp = dist[neighbourNode]
                    if (alt!! < dist[neighbourNode]!!) {
                        dist[neighbourNode] = alt
                        prev[neighbourNode] = node

                        pq.remove(Pair(neighbourNode, tmp))
                        val priority = when(algo){
                            ShortestPathAlgorithms.DIJKSTRA -> dist[neighbourNode]!!
                            ShortestPathAlgorithms.ASTAR -> dist[neighbourNode]!! + heuristic!!(neighbourNode, dest!!)
                        }
                        pq.add(Pair(neighbourNode, priority))
                    }
                }
        }
        return Pair(dist, prev)
    }

    fun tspNNH(root: T): Pair<List<T>, Float> {
        val completeGraph = castToCompleteGraph(this)
        val visited = HashSet<T>()

        fun recNNH(s: T): List<T> {
            if (completeGraph.vertices.all { visited.contains(it) })
                return listOf(root)

            visited.add(s)
            val nextNode = completeGraph.edges[s]!!
                .filter { !visited.contains(it.first) }
                .minByOrNull { it.second } ?: Pair(root, 0f)

            return listOf(s) + recNNH(nextNode.first)
        }

        val circuitPath = recNNH(root)
        val cost = pathCost(this, circuitPath)

        return if (cost == Float.POSITIVE_INFINITY) {
            Pair(listOf(), cost)
        } else {
            Pair(circuitPath, cost)
        }
    }

    fun mstPrim(root: T): UndirectedGraph<T> {
        val q = PriorityQueue<Triple<T, T, Float>>(compareBy { it.third })
        val visited = hashSetOf(root)
        edges[root]!!.forEach { q.add(Triple(root, it.first, it.second)) }

        val mst = UndirectedGraph(vertices, listOf())
        var currentNode: T
        while (q.size > 0 && visited.size < vertices.size) {
            val edge = q.poll()
            if (visited.contains(edge.second))
                continue

            mst.addEdges(edge)
            currentNode = edge.second
            visited.add(currentNode)

            edges[currentNode]!!
                .filter { !visited.contains(it.first) }
                .forEach { q.add(Triple(currentNode, it.first, it.second)) }
        }
        return mst
    }

    fun cliqueBronKerbosch(): Set<List<T>> {
        val allCliques = HashSet<List<T>>()

        fun bk(candidateClique: HashSet<T>, remainingNodes: HashSet<T>, skipNodes: HashSet<T>) {
            if (remainingNodes.isEmpty() && skipNodes.isEmpty())
                allCliques.add(candidateClique.toList())
            else {
                // can't use remainingNodes.forEach because remainingNodes is modified inside the loop
                val iterator = remainingNodes.iterator()
                while (iterator.hasNext()) {
                    val currentNode = iterator.next()
                    val nx = neighbours(currentNode)
                    val newR = candidateClique.toHashSet()
                    newR.add(currentNode)
                    bk(newR, remainingNodes.intersect(nx) as HashSet<T>, skipNodes.intersect(nx) as HashSet<T>)
                    // remove currentNode from remainingNodes
                    iterator.remove()
                    skipNodes.add(currentNode)
                }
            }
        }

        bk(HashSet(), vertices.toHashSet(), HashSet())
        return allCliques
    }
}
