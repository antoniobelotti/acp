package base

class DirectedGraph<T>(
    elements: MutableList<T>,
    edgesList: List<Triple<T, T, Float>>
) : Graph<T>(elements, edgesList, false) {

    fun topologicalOrdering(): List<T> {
        val v = vertices.toMutableList()
        val e = edges.toMutableMap()

        val result = mutableListOf<T>()
        val s = HashSet<T>()

        fun updateS() {
            val candidates = v.toMutableList()
            e.forEach { (_, edges) ->
                edges.forEach { (edge, _) ->
                    candidates.remove(edge)
                }
            }
            s.addAll(candidates)
        }

        while (result.size < vertices.size) {
            updateS()
            val n = s.first()

            // remove n from the to-do node lists
            s.remove(n)
            v.remove(n)

            result.add(n)
            // remove all outgoing edges from n
            e.remove(n)
        }

        if (e.isNotEmpty()) {
            return listOf()
        }
        return result
    }

    fun floydWarshall(): HashMap<Pair<T, T>, List<T>> {
        val dist = HashMap<Pair<T, T>, Float>()
        val next = HashMap<Pair<T, T>, T?>()

        vertices.forEach { src ->
            vertices.forEach { dest ->
                var edge:Pair<T,Float>?
                try {
                    edge = edges[src]?.first { (n, _) -> n == dest }
                }catch (e:Exception ){
                    edge = null
                }
                dist[Pair(src, dest)] = edge?.second ?: Float.POSITIVE_INFINITY
                next[Pair(src, dest)] = edge?.first
            }
        }
        vertices.forEach {
            dist[Pair(it, it)] = 0f
            next[Pair(it, it)] = it
        }

        vertices.forEach { k ->
            vertices.forEach { i ->
                vertices.forEach { j ->
                    if (dist[Pair(i, j)]!! > dist[Pair(i, k)]!! + dist[Pair(k, j)]!!) {
                        dist[Pair(i, j)] = dist[Pair(i, k)]!! + dist[Pair(k, j)]!!
                        next[Pair(i, j)] = next[Pair(i, k)]!!
                    }
                }
            }
        }

        fun path(node1: T, node2: T): List<T> {
            var x = node1
            var y = node2

            if (next[Pair(x, y)] == null)
                return listOf()
            val res = mutableListOf(x)
            while (x != y) {
                x = next[Pair(x, y)]!!
                res.add(x)
            }
            return res
        }

        val res = HashMap<Pair<T, T>, List<T>>()
        vertices.forEach { node1 ->
            vertices.filter { it != node1 }.forEach { node2 ->
                res[Pair(node1, node2)] = path(node1, node2)
            }
        }
        return res
    }
}