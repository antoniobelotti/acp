package base


fun <T> castToCompleteGraph(graph: UndirectedGraph<T>): UndirectedGraph<T> {
    graph.edges.forEach { (srcNode, edgeList) ->
        val candidates = graph.vertices.toMutableList()
        edgeList.forEach { candidates.remove(it.first) }
        edgeList.addAll(candidates.filter { it!=srcNode }.map { Pair(it, Float.POSITIVE_INFINITY) })
    }
    return graph
}

fun <T> pathCost(graph: Graph<T>, path: List<T>): Float {
    var cost = 0f
    (0..path.size-2).forEach {  idx ->
        val srcNode = path[idx]
        val destNode = path[idx+1]
        cost += graph.edges[srcNode]!!.find { it.first==destNode }!!.second
    }
    return cost
}