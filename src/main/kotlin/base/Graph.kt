package base


import kotlinx.coroutines.sync.*
import java.util.*
import kotlin.system.*
import kotlin.collections.HashMap
import kotlin.collections.HashSet

abstract class Graph<T>(
    val vertices: MutableList<T>,
    edgesList: List<Triple<T, T, Float>>,
    private val undirected: Boolean
) {

    val edges = HashMap<T, MutableList<Pair<T, Float>>>()

    init {
        edgesList.forEach {
            val (from, to, weight) = it

            if (!this.edges.containsKey(from))
                this.edges[from] = mutableListOf()
            this.edges[from]!!.add(Pair(to, weight))

            if (undirected) {
                if (!this.edges.containsKey(to))
                    this.edges[to] = mutableListOf()
                this.edges[to]!!.add(Pair(from, weight))
            }
        }
    }

    @Synchronized
    fun addVertices(vararg nodes: T) = vertices.addAll(nodes)

    // assert vertices are already in the graph
    @Synchronized
    fun addEdges(vararg newEdges: Triple<T, T, Float>) {
        newEdges.forEach {
            if (!edges.containsKey(it.first))
                edges[it.first] = mutableListOf()
            edges[it.first]!!.add(Pair(it.second, it.third))

            if (undirected) {
                if (!edges.containsKey(it.second))
                    edges[it.second] = mutableListOf()
                edges[it.second]!!.add(Pair(it.first, it.third))
            }
        }
    }

    @Synchronized
    fun removeEdge(from: T, to: T): Boolean {
        /* Removes any edge (from-->to). If the graph is undirected it removes the specular edges (to-->from).
           Returns true on success, false if no element was removed */
        var result = edges[from]?.removeIf { (dest, _) -> dest == to } ?: false
        if (undirected)
            result = result && edges[to]?.removeIf { (dest, _) -> dest == from } ?: false
        return result
    }

    @Synchronized
    fun removeEdge(edge: Triple<T, T, Float>): Boolean {
        /* Removes the specified edge. If the graph is undirected it removes the specular edge.
           Returns true on success, false if no element was removed */
        val (from, to, weight) = edge
        var result = edges[from]?.remove(Pair(to, weight)) ?: false
        if (undirected)
            result = result && edges[to]?.remove(Pair(from, weight)) ?: false
        return result
    }

    fun neighbours(node: T): Set<T> = edges[node]!!.map { it.first }.toSet()

    fun depthFirstVisit(root: T): List<T> {
        val visited: HashSet<T> = HashSet()

        fun dfs(root: T): List<T> {
            if (!visited.add(root)) return listOf()

            if (!edges.containsKey(root) || edges[root]!!.none { !visited.contains(it.first) }) {
                return listOf(root)
            }

            val result = mutableListOf<T>()
            edges[root]!!.forEach { result.addAll(dfs(it.first)) }

            result.add(root)
            return result
        }

        return dfs(root)
    }

    fun depthFirstSearchBool(root: T, target: T): Boolean {
        val visited: HashSet<T> = HashSet()

        fun dfs(root: T): Boolean {
            if (!visited.add(root)) return false
            if (root == target) return true
            if (!edges.containsKey(root) || edges[root]!!.all { visited.contains(it.first) }) {
                return false
            }

            var result = false
            edges[root]!!.forEach { result = result || dfs(it.first) }
            return result
        }

        return dfs(root)
    }

    fun depthFirstReturnPath(root: T, dest: T): List<T>? {
        val visited: HashSet<T> = HashSet()

        fun dfs(root: T): List<T>? {
            if (!visited.add(root)) return null
            if (root == dest) return listOf(root)
            if (!edges.containsKey(root) || edges[root]!!.all { visited.contains(it.first) }) {
                return null
            }

            var result = mutableListOf(root)
            edges[root]!!.forEach { (neighbour, _) ->
                val path = dfs(neighbour)
                if (path != null) {
                    result.addAll(path)
                    return@forEach
                }
            }
            if (result.size > 1)
                return result
            return null
        }

        return dfs(root)
    }

    fun breadthFirstVisit(root: T): List<T> {
        val visited = HashSet<T>()
        val queue = mutableListOf<T>()
        queue.add(root)
        val result = mutableListOf<T>()
        while (queue.size > 0) {
            val e = queue.removeFirst()
            visited.add(e)
            result.add(e)
            val vertices = edges[e]!!.map { it.first }.filter { !visited.contains(it) }
            queue.addAll(vertices)
        }
        return result.toList()
    }

    fun breadthFirstReturnPath(src: T, dest: T): MutableList<T> {
        /* invert search to simplify buildPath backtracking */
        val start = dest
        val end = src

        val visited = HashSet<T>()
        val parent = HashMap<T, T>()
        val queue = LinkedList<T>()
        queue.add(start)

        fun buildPath(destNode:T): MutableList<T> {
            val path = mutableListOf(destNode)
            var currentNode = destNode
            while(currentNode!=start) {
                path.add(parent[currentNode]!!)
                currentNode = parent[currentNode]!!
            }
            return path
        }

        var currentNode=start
        while (queue.size > 0) {
            currentNode = queue.removeFirst()
            if (currentNode == end) {
                break
            }
            visited.add(currentNode)
            val vertices = edges[currentNode]!!.map { it.first }.filter { !visited.contains(it) }
            vertices.forEach { parent[it] = currentNode }
            queue.addAll(vertices)
        }
        return buildPath(currentNode)
    }

    /* used for example by PriorityQueue to order edges */
    protected fun edgeComparator(x: Pair<T, Float>, y: Pair<T, Float>): Int {
        return when {
            x.second < y.second -> -1
            x.second > y.second -> 1
            else -> 0
        }
    }

    override fun toString(): String {
        return "Graph(elements=$vertices, edges=$edges)"
    }
}